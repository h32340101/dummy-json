package com.example.dummy_json

import com.google.gson.Gson
import com.google.gson.JsonParser

class EmployeeSerializer {
    fun fromJson(json: String): EmployeeJson {
        val jsonElement = JsonParser.parseString(json)
        return Gson().fromJson(jsonElement, EmployeeJson::class.java)
    }
}

data class EmployeeJson(
    val status: String = "",
    val data: List<EmployeeDataJson> = listOf(),
    val message: String = "",
)

data class EmployeeDataJson(
    val id: Int = 0,
    val employee_name: String = "",
    val employee_salary: Int = 0,
    val employee_age: Int = 0,
    val profile_image: String = "",
)

