package com.example.dummy_json

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_employee.*
import kotlinx.coroutines.*
import java.io.FileNotFoundException
import java.net.URL

class EmployeeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee)

        employeeRecycler.layoutManager =
            LinearLayoutManager(
                this@EmployeeActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        employeeRecycler.setHasFixedSize(true)
        CoroutineScope(Dispatchers.Main).launch {
            showDummyJson()
        }

        syncButton.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                employeeRecycler.adapter?.run {
                    employeeRecycler.adapter = null
                    delay(200)
                }
                showDummyJson()
                syncButton.isEnabled = true
            }
            syncButton.isEnabled = false
        }
    }

    private suspend fun showDummyJson() {
        val json = withContext(Dispatchers.IO) {
            Log.d(TAG, "非同步執行順序 debug: 發出 request")
            getDummyJson()
        } ?: run {
            AlertDialog.Builder(this@EmployeeActivity)
                .setTitle("讀取網路資料失敗")
                .setNeutralButton("ok", null)
                .show()
            return
        }
        val employeeJson = EmployeeSerializer().fromJson(json)
        val data = employeeJson.data
        val adapter = EmployeeAdapter(data)
        employeeRecycler.adapter = adapter
        Log.d(TAG, "非同步執行順序 debug: 設定 adapter")
    }

    private fun getDummyJson(): String? {
        return try {
            URL("http://dummy.restapiexample.com/api/v1/employees").readText()
                .also { Log.d(TAG, "非同步執行順序 debug: 取得 response") }
        }
        catch (ex: FileNotFoundException) {
            Log.d(TAG, "非同步執行順序 debug: 未取得 response")
            null
        }
    }

    companion object {
        private const val TAG = "EmployeeActivity"
    }
}