package com.example.dummy_json

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.dummy_json.databinding.EachEmployeeDetailsBinding
import kotlinx.android.synthetic.main.each_employee_details.view.*

class EmployeeAdapter(
    private val employee: List<EmployeeDataJson>
) : RecyclerView.Adapter<EmployeeAdapter.MyViewHolder>() {

    // 使用 view binding, 避免不同 layout 但有相同 id 的 view, 以 findViewById() 取得實體會有問題
    class MyViewHolder(itemView: EachEmployeeDetailsBinding)
        : RecyclerView.ViewHolder(itemView.root) {

        fun applyData(data: EmployeeDataJson) {
            data.apply {
                id             .also { itemView.idTextView.text     = it.toString() }
                employee_name  .also { itemView.nameTextView.text   = it }
                employee_salary.also { itemView.salaryTextView.text = it.toString() }
                employee_age   .also { itemView.ageTextView.text    = it.toString() }
                profile_image  .also { itemView.imageTextView.text  = it }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = EachEmployeeDetailsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.applyData(employee[position])
    }

    override fun getItemCount(): Int {
        return employee.size
    }
}